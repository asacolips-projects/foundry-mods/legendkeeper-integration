import { LegendKeeperFilePicker } from './lib/LegendKeeperFilePicker.js';
export class LegendKeeperFs {
  /**
   * Constructor for the class.
   *
   * Retrieves settings and sets up instance properties that will be used during
   * the import process.
   */
  constructor() {
    // Retrieve settings.
    let settings = game.settings.get('legendkeeper-integration', 'importDirectory');
    if (typeof settings !== 'string') settings = '';

    // Instantiate the file picker.
    let pickerOptions = LegendKeeperFilePicker.parse(settings);
    let picker = new LegendKeeperFilePicker(pickerOptions);

    // Determine our file system (user data, s3, Forge).
    let fs = picker.activeSource ?? 'data';
    let path = picker.request ?? '';
    let bucket = fs == 's3' ? picker.sources.s3.bucket : null;

    // Handle pathing for local vs non-local paths.
    if (fs == 'data') {
      path = (path && path[0] != '/') ? `/${path}` : path;
    }

    // Source file properties.
    this.origin = window.location.origin;
    this.source = fs;
    this.bucket = bucket;
    this.path = path ? path : '';
    // Sorting properties.
    this.tree = [];
    this.sorted = {};
    this.handled = [];
    this.parents = [];
    // Dialog and misc properties.
    this.dialog = null;
    this.scheduledUpdates = [];
    this.killAll = false;
    this.keyJournalData = [];
  }

  /**
   * Fetch a JSON file for the importer.
   * @param {string} filename
   * @param {boolean} skipHandled
   * @returns
   */
  async fetchJson(filename, skipHandled = true) {
    // Exit early if this flag is set.
    if (this.killAll) {
      return false;
    }

    // Handle relative paths.
    let path = filename;
    if (!filename.includes('http')) {
      path = `${this.origin}${filename[0] != '/' ? '/' : ''}${filename}`;
    }

    let response = null;
    // Handle imports
    if (!skipHandled || this.handled.includes(filename) == false) {
      // Try to fetch the project's export JSON.
      try {
        response = await fetch(path);
        if (response.status !== 200) {
          return false;
        }
        // Response was successful, set the tree to the resources property.
        else {
          let json = await response.json();
          this.tree = json.resources;
          return true;
        }
      } catch (error) {
        return false;
      }
    }
    return false;
  }

  /**
   * Sort entries.
   *
   * If the optional setting for this is enabled, entries will be sorted by
   * name instead of their import order.
   *
   * @returns array
   *   Returns sorted entries.
   */
  sortEntries() {
    // Handle top-level first.
    let result = {
      content: null,
      items: []
    };

    let sort = game.settings.get('legendkeeper-integration', 'sortOnImport');

    // Filter to top-level documents first.
    this.tree.filter(entry => entry.parentId == null).forEach(entry => {
      // Query tree also has its own sort.
      this.queryTree(entry, result['items'], sort);
    });

    // Sort items by name.
    if (sort) {
      result['items'] = result['items'].sort((a, b) => {
        const aSort = a.content.name.toLowerCase().replace(/[^a-zA-Z\d]/g, '');
        const bSort = b.content.name.toLowerCase().replace(/[^a-zA-Z\d]/g, '');

        if (aSort < bSort) {
          return -1;
        }
        if (aSort > bSort) {
          return 1;
        }
        return 0;
      });
    }

    return result;
  }

  /**
   * Query the subtree for this entry.
   *
   * Entries are stored in the export JSON at their top level, with the
   * exception of tabs (which are nested in a given entry). We need to query
   * them via id/parentId to determine the full tree for a given entry.
   *
   * @param {object} entry
   *   The entry we're building the tree for.
   * @param {array} sortedEntries
   *   Array of sorted entries to query from.
   * @param {boolean} sort
   *   Whether or not to sort by name.
   *
   * @returns mixed
   *   False if the killAll flag was enabled.
   */
  queryTree(entry, sortedEntries, sort = false) {
    if (this.killAll) {
      return false;
    }

    // Query sub entries.
    let items = this.tree.filter(item => item.parentId == entry.id);
    sortedEntries.push({
      content: entry,
      items: []
    });

    let index = sortedEntries.length - 1;

    // Iterate through items and recursively query them.
    items.forEach(item => {
      let children = this.tree.filter(treeItem => treeItem.parentId == item.id);
      if (children.length > 0) {
        this.queryTree(item, sortedEntries[sortedEntries.length - 1]['items']);
      }
      else {
        sortedEntries[sortedEntries.length - 1]['items'].push({
          content: item,
          items: []
        });
      }
    });

    // Sort entries if needed.
    if (sort) {
      sortedEntries[index]['items'] = sortedEntries[index]['items'].sort((a, b) => {
        const aSort = a.content.name.toLowerCase().replace(/[^a-zA-Z\d]/g, '');
        const bSort = b.content.name.toLowerCase().replace(/[^a-zA-Z\d]/g, '');

        if (aSort < bSort) {
          return -1;
        }
        if (aSort > bSort) {
          return 1;
        }
        return 0;
      });
    }
  }

  /**
   * Fetch JSON to import.
   *
   * @returns mixed
   *   False on failure.
   */
  async fetchAll() {
    if (this.path.length < 1 || !this.path.includes('.json')) {
      ui.notifications.error(game.i18n.localize('LK_INTEGRATION.error.noDir'));
      return false;
    }

    // Load the file dir.
    let opt = this.bucket ? {bucket: this.bucket} : {};
    let dir = await FilePicker.browse(this.source, this.path, opt);

    // Load the file.
    let success = await this.fetchJson(this.path);
    if (!success) {
      return false;
    }

    // Sort entries if necessary.
    this.sorted = this.sortEntries();

    return true;
  }

  /**
   * Preserve journal data before imports.
   *
   * When new imports happen, we delete and replace them. This accounts for that
   * by storing an array of journal entry IDs and their permissions so that we
   * can later restore those on import.
   */
  async keepKeyJournalData() {
    this.keyJournalData = [];
    let journals = game.journal.filter(j => j.flags.legendkeeper != undefined);

    for (let journal of journals) {
      this.keyJournalData[journal.flags.legendkeeper.id] = { permission: journal.permission};
    }
  }

  /**
   * Import entries and sub entries.
   *
   * Recursive function to import entries as new journal documents.
   *
   * @param {array} items
   *   Array of entries to import.
   * @param {string|null} parent
   *   String for the ID of the parent (if any).
   * @param {string|null} nestedParent
   *   String for the ID of the parent's parent (if any).
   * @param {number} level
   *   Integer to represent how deep this entry is (max level 3, additional
   *   levels are simulated with "-" characters).
   * @param {string} prefix
   *   Prefix to use for entries, if any.
   *
   * @returns mixed
   *   False on failure.
   */
  async importEntries(items, parent = null, nestedParent = null, level = 0, prefix = '') {
    if (this.killAll) {
      return false;
    }

    if (items) {
      // Keep track of how many entries have been imported.
      let count = 0;

      // If there isn't a parent (meaning this is a top level entry),
      // initialize the import. Store important data, clear the existing
      // entries, and start a progress bar.
      if (!parent) {
        await this.keepKeyJournalData();
        await this.deleteEntries();
        game.LegendKeeper.updating = true;

        // The article import doesn't do a per article progress, so we're using
        // +10 here to show the user that there's still a significant step
        // left over while waiting on that task to complete.
        // @deprecated - the Hydra import is fast enough that we likely don't
        // need this anymore!
        this.renderProgress({ current: 0, max: items.length + 10, details: 'LK_INTEGRATION.import.importingFolders', update: true });
        count = 0;
      }

      // Iterate through items to import.
      for (let item of items) {
        if (this.killAll) {
          return false;
        }

        // Retrieve keys for iteration.
        let keys = Object.keys(item.items);
        // Handle entries with sub-entries.
        if (keys.length > 0 || parent == null) {
          // Build the prefix string based on the current level.
          let levelPrefix = '';
          for (let i = 2; i < level; i++) {
            levelPrefix += '—';
          }


          // For levels greater than 3, adjust the prefix.
          if (level >= 3) {
            prefix = prefix == '' ? this.cleanPrefix(item.content.name) : `${prefix}${this.cleanPrefix(item.content.name)}`;
          }

          // Create a folder if possible.
          let folder = await Folder.create({
            name: `[LK]${level >= 2 ? prefix : ''} ${item.content.name}`,
            type: 'JournalEntry',
            parent: parent,
            "flags.legendkeeper.id": item.content.id
          });

          // Import this entry, and then import its sub-entries.
          await this.importEntry(item, folder.id, level, true);
          await this.importEntries(item.items, level < 2 ? folder.id : parent, folder.id, level + 1, prefix);
        }
        // Handle singular entries.
        else {
          await this.importEntry(item, nestedParent, level);
        }

        // If this entry didn't have a parent (meaning its not a sub-entry),
        // update the progress indicator.
        if (!parent) {
          count++;
          this.renderProgress({ current: count, max: items.length + 10, details: 'LK_INTEGRATION.import.importingFolders', update: true });
        }
      }

      // Now that the updates array has been built, update our progress and
      // create the new entries.
      if (!parent) {
        if (this.scheduledUpdates.length > 0) {
          count++;
          this.renderProgress({ current: count, max: items.length + 10, details: 'LK_INTEGRATION.import.importingFolders', update: true });
          await JournalEntry.create(this.scheduledUpdates);
        }

        game.LegendKeeper.updating = false;
        if (this.dialog) this.dialog.close();
      }
    }
  }

  async importEntry(item, parent = null, level = 0, rootArticle = false) {
    if (this.killAll) {
      return false;
    }

    let content = null;
    // If there's an image for the article, add it as a tab.
    if (item.content?.imageUrl) {
      let imgId = foundry.utils.randomID();
      item.content.documents.push({
        id: imgId,
        name: game.i18n.localize('Image'),
        content: `<div class='lk-tab' id='${imgId}'><img src='${item.content.imageUrl}' /></div>`
      });
    }
    // Handle tabs.
    // @todo refactor this to use journal pages instead of custom tabs.
    if (item.content.documents.length > 1) {
      let tabs = '';
      let tabContents = '';
      let firstTab = true;
      for (let entry of Object.values(item.content.documents)) {
        tabs += `<li class="lk-tabs-control"><button class="lk-tabs-control-link${firstTab ? ' active' : ''}" id="${entry.id}">${entry.name ?? 'Main'}</button></li>`
        tabContents += firstTab ? entry.content.replace("class='lk-tab'", "class='active lk-tab'") : entry.content;
        firstTab = false;
      }
      content = `<div class="lki-content">`;
      content += `<ul class="lk-tabs-controls">${tabs}</ul>`;
      content += `<div class="lk-tabs-contents">${tabContents}</div>`;
      content += `</div>`;
    }
    // Handle documents without tabs.
    else if (item.content.documents.length == 1) {
      content = item.content.documents.map(i => i.content).join('<hr>');
    }
    // Build the content array.
    let $content = [];
    if (content) {
      $content = $(`<div>${content}</div>`);
      // @todo Secrets are not yet included in Hydra exports.
      let $secrets = $content.find('[data-extension-key="block-secret"]');
      if ($secrets.length > 0) {
        $secrets.each((index, element) => {
          let $self = $(element);
          if ($self.length > 0) {
            let html = $self.html();
            let $secret = $(`<section class="secret"></section>`);
            $secret.html(html);
            $self.replaceWith($secret[0]);
          }
        });
      }
    }

    // Build the entry document object.
    let entry = {
      name: `[LK] ${rootArticle ? '[' + game.i18n.localize('LK_INTEGRATION.rootArticle') + ']------------------------------------' : item.content.name}`,
      content: $content && $content.length > 0 ? $content.html() : content,
      type: 'JournalEntry',
      folder: parent,
      "flags.legendkeeper.id": item.content.id,
      "rlags.legendkeeper.root": rootArticle,
    }

    // Assign permissions if we have them from an earlier import.
    if (this.keyJournalData[item.content.id] && this.keyJournalData[item.content.id].permission != undefined) {
      entry.permission = this.keyJournalData[item.content.id].permission;
    }

    // Push this document to our updates array for creation later.
    this.scheduledUpdates.push(entry);
  }

  /**
   * Delete pre-existing entries.
   *
   * This method is called to clear out pre-existing entries prior to a new
   * execution of the importer. To simplify the import process, all articles
   * flagged as LK articles will be deleted so that they can be imported as new
   * documents.
   *
   * @returns mixed
   *   False on failure.
   */
  async deleteEntries() {
    // Query folders.
    let folders = game.folders.filter(f => f.type == 'JournalEntry' && f.parent == null && f.flags.legendkeeper != undefined);
    this.renderProgress({ current: 0, max: folders.length - 1, details: 'LK_INTEGRATION.import.deletingEntries', update: true });
    let count = 0;

    // Iterate through folders, deleting their contents.
    for (let folder of folders) {
      if (this.killAll) {
        return false;
      }

      await folder.delete({ deleteSubfolders: false, deleteContents: true });
      count++;
      this.renderProgress({ current: count, max: folders.length - 1, details: 'LK_INTEGRATION.import.deletingEntries', update: true });
    }
    // Delete journal entries.
    let journals = game.journal.filter(j => j.flags.legendkeeper != undefined && j.folder == null);
    for (let journal of journals) {
      await journal.delete();
    }
  };

  /**
   * Add hyperlinks to LK journal entries.
   *
   * Articles can have links inside them to LK articles. Once the journal
   * entries have been imported, this will be executed to go over them and
   * update any LK links to their matching Foundry journals, if possible.
   */
  async updateLinks() {
    // Query for journal entries.
    let journals = game.journal.filter(j => j.flags.legendkeeper != undefined);
    let updates = [];

    for (let journal of journals) {
      // @todo refactor this better handle multiple pages.
      let firstPage = null;
      journal.pages.forEach(page => firstPage = page);

      // If the entry has content in it, we need to check it for URLs.
      if (firstPage.text.content && firstPage.text.content.includes('.html')) {
        let content = $(firstPage.text.content);
        if (content.length > 0) {
          // Iterate over URLs in the entries.
          content.find('a').each((index, elem) => {
            let $self = $(elem);
            let url = $self.attr('href');
            let text = $self.text();
            let journalEntry = game.journal.find(j => j.flags?.legendkeeper?.id == url.split('.')[0]);
            // Update the URL with a Foundry link.
            if (journalEntry) {
              $self.replaceWith(`@JournalEntry[${journalEntry.id}]{${text}}`);
            }
            // Throw a warning if no entry was found.
            else {
              console.warn(`Unable to find linked entry for ${text} in article ${journal.name}. This entry was most likely deleted in your Legend Keeper world. Converting this link into plain text.`);
              $self.replaceWith(text);
            }
          });

          firstPage.update({
            'text.content': content.html()
          });
        }
      }
    }
  }

  /**
   * Render the progress bar.
   *
   * Render the progress bar, or update the progress of an existing progress
   * bar dialog.
   *
   * @param {object} options
   *   current: Integer, current step of the loop.
   *   max: Integer, maximum possible step.
   *   details: String, message to show.
   *   update: Boolean, whether to render a new dialog or update the current bar
   * @returns
   */
  renderProgress({ current = 0, max = 0, details = '', update = false }) {
    // If we've hit the end, return false.
    if (current == max) {
      return false;
    }

    // If update is false, we need to create a new dialog.
    if (!update) {
      this.dialog = new Dialog({
        title: 'Importing Legend Keeper...',
        content: `<div class="lk-progress-track"><div class="lk-progress-bar" style="width:${max > 0 ? (current / max) * 100 : 0}%;"></div></div>${details ? '<h2 class="lk-progress-title">' + game.i18n.localize(details) + '</h2>' : ''}`,
        buttons: {
          cancel: {
            icon: '<i class="fas fa-times"></i>',
            label: game.i18n.localize("Cancel"),
            callback: () => null
          },
        },
        close: () => this.killAll = true,
      }, {
        width: 480,
        height: 140,
      }).render(true);
    }
    // Otherwise, update the existing one.
    else {
      $(document).find('.lk-progress-bar').css('width', `${(current / max) * 100}%`);
      $(document).find('.lk-progress-title').text(game.i18n.localize(details));
    }
  }

  cleanPrefix(string) {
    // TODO: The original version of this function may be useful for sorting,
    // but for now, it's disabled.
    // return string.replace(/( +)/g, ' ').replace(/[^a-zA-Z\d ]/g, '').toUpperCase().split(' ').map(s => s[0]).join('');
    return '—';
  }
}