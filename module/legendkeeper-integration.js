import { LegendKeeperFilePicker } from "./lib/LegendKeeperFilePicker.js";
import { LegendKeeperFs } from './legendkeeper-fs.js';

Hooks.once('init', async function() {

  game.LegendKeeper = {
    fs: LegendKeeperFs,
    fp: LegendKeeperFilePicker,
    updating: false
  };

  game.settings.register('legendkeeper-integration', 'importDirectory', {
    name: 'LK_INTEGRATION.importDirectory.name',
    hint: 'LK_INTEGRATION.importDirectory.hint',
    scope: 'world',
    config: true,
    type: LegendKeeperFilePicker.Directory,
    default: '',
  });

  game.settings.register('legendkeeper-integration', 'sortOnImport', {
    name: 'LK_INTEGRATION.setting.sortOnImport.name',
    hint: null,
    scope: 'world',
    config: true,
    default: false,
    type: Boolean,
  });

  console.log('LEGEND KEEPER INTEGRATION ENABLED');
});

Hooks.once('ready', function() {
  // Fix broken settings.
  let lkSetting = game.settings.get('legendkeeper-integration', 'importDirectory');
  if (lkSetting && lkSetting.startsWith('[') === false) {
    game.settings.set('legendkeeper-integration', 'importDirectory', '');
    ui.notifications.warn('LK_INTEGRATION.warn.v2_0_0', {
      permanent: true,
      localize: true
    });
  }
});

Hooks.on('renderJournalDirectory', async (app, html, options, id) => {
  if (!game.LegendKeeper.updating) {
    html.find('.folder h3, .journal h4').each((index, element) => {
      let $self = $(element);
      let content = $self.html();

      if (content.includes('[LK]')) {
        content = content.replace(/\[LK\]/g, '<span class="lk-icon"></span>');

        // TODO: All of this is potentially obsolete, so it's been disabled.
        // content = content.replace(/\[\|(.*)\|\]/g, (match) => {
        //   let result = `<span class="visually-hidden">${match}</span>` + match
        //     .replace(/[\[\]\|]/g, '')
        //     .replace(/^[a-zA-Z\d]+\/*/g, '')
        //     .replace(/[a-zA-Z\d]+\/*/g, '—');
        //   return result;
        // });
        // $self.html(content);

        // if ($self.hasClass('entity-name')) {
        //   let $parent = $self.closest('.folder').find('h3');

        //   if ($self.text() == $parent.text()) {
        //     $self.find('a').html(`<span class="lk-icon"></span><span>Article</span>`);
        //     let $entry = $self.closest('.journal');
        //     $entry.parent().prepend($entry);
        //   }
        // }

        $self.html(content);
      }
    });
  }

  if (!game.user.isGM) return;

  // Add the World Anvil Button
  const button = $(`<button type="button" id="lk-import">
    <img src="modules/legendkeeper-integration/assets/icons/lk.svg" title="${game.i18n.localize('LK_INTEGRATION.buttons.import')}"/> ${game.i18n.localize('LK_INTEGRATION.buttons.import')}
  </button>`);
  button.on("click", async ev => {
    let lk = new LegendKeeperFs();
    let success = await lk.fetchAll();

    if (success) {
      // Import our entries.
      await lk.importEntries(lk.sorted.items);
      // Once they're imported, update the hyperlinks in them.
      await lk.updateLinks();
      // Update our sidebar with the new content.
      ui.sidebar.render();
    }
  });
  html.find(".directory-footer").append(button);
});

Hooks.on('renderJournalPageSheet', async (app, html, options) => {
  let hasTabs = html.find('.lk-tabs-controls');
  if (!hasTabs || hasTabs.length < 1) {
    return;
  }

  let tabButtons = html.find('.lk-tabs-control-link');
  let tabs = html.find('.lk-tab');

  tabButtons.on('click', (event) => {
    event.preventDefault();
    let button = $(event.currentTarget);
    tabButtons.removeClass('active');
    button.addClass('active');

    let id = button.attr('id');
    let tab = html.find(`.lk-tab#${id}`);
    tabs.removeClass('active');
    tab.addClass('active');
  });
});