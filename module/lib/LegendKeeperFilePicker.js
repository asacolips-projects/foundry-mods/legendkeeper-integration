/**
 * Game Settings: Directory
 *
 * Originally based on https://github.com/MrPrimate/ddb-importer/blob/1136ea5e11dcb4406c5ae4890aefd73c53addabd/src/lib/LegendKeeperFilePicker.js
 */

export class LegendKeeperFilePicker extends FilePicker {
  constructor(options = {}) {
    // Parent constructor.
    super(options);
    // Merge in passed in arguments.
    mergeObject(this, options);
  }

  /** @inheritdoc */
  _getExtensions(type) {
    // Only allow JSON files.
    return ['.json', '.JSON'];
  }

  /**
   * Process submit event and set the value.
   * @param {Event} event
   */
  _onSubmit(event) {
    event.preventDefault();
    const path = event.target.file.value;
    const activeSource = this.activeSource;
    const bucket = event.target.bucket ? event.target.bucket.value : null;
    this.field.value = LegendKeeperFilePicker.format({
      activeSource,
      bucket,
      path,
    });
    this.close();
  }

  /** @inheritdoc */
  static async uploadToPath(path, file) {
    const options = LegendKeeperFilePicker.parse(path);
    return FilePicker.upload(options.activeSource, options.current, file, { bucket: options.bucket });
  }

  /**
   * Returns the type "Directory" for rendering the SettingsConfig.
   */
  static Directory(val) {
    return val;
  }

  /**
   * Formats the data into a string for saving it as a GameSetting.
   */
  static format(value) {
    return value.bucket !== null
      ? `[${value.activeSource}:${value.bucket}] ${value.path}`
      : `[${value.activeSource}] ${value.path}`;
  }

  /**
   * Parses the string back to something the FilePicker can understand as an option.
   */
  static parse(str) {
    let matches = str.match(/\[(.+)\]\s*(.+)/);
    if (matches) {
      let source = matches[1];
      const current = matches[2].trim();
      const [dataSource, bucket] = source.split(":");
      // S3 files.
      if (bucket !== undefined) {
        return {
          activeSource: dataSource,
          bucket: bucket,
          current: current,
        };
      }
      // Other file sources (Forge, user data.)
      else {
        return {
          activeSource: dataSource,
          bucket: null,
          current: current,
        };
      }
    }
    // Fallback to user data otherwise.
    return {
      activeSource: "data",
      bucket: null,
      current: str,
    };
  }

  /**
   * Adds a FilePicker-Simulator-Button next to the input fields.
   */
  static processHtml(html) {
    $(html)
      .find(`input[data-dtype="Directory"]`)
      .each((index, element) => {
        // disable the input field raw editing
        // $(element).prop("readonly", true);

        // if there is no button next to this input element yet, we add it
        if (!$(element).next().length) {
          let picker = new LegendKeeperFilePicker({
            field: $(element)[0],
            ...LegendKeeperFilePicker.parse($(element).val()),
          });
          let pickerButton = $(
            '<button type="button" class="file-picker" data-type="imagevideo" data-target="img" title="Pick directory"><i class="fas fa-file-import fa-fw"></i></button>'
          );
          pickerButton.on("click", () => {
            picker.render(true);
          });
          $(element).parent().append(pickerButton);
        }
      });
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);
  }
}

// this is hooked in, we don't use all the data, so let's stop eslint complaining
// eslint-disable-next-line no-unused-vars
Hooks.on("renderSettingsConfig", (app, html, user) => {
  LegendKeeperFilePicker.processHtml(html);
});
